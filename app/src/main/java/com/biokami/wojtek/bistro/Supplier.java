package com.biokami.wojtek.bistro;

/**
 * Created by nirav on 21/02/16.
 */
public class Supplier {

    private static int counter = 0;

    public final int objectId;

    private String phone;

    private String email;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Supplier(String name, String phone, String email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.objectId = counter++;
    }
}
