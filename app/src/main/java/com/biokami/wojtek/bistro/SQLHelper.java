package com.biokami.wojtek.bistro;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jack on 5/10/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SQLHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "bistro.sqlite";
    private static final int VERSION = 1;


    public SQLHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the "quotes" table
        db.execSQL("create table products (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, price INTEGER, supplier_id INTEGER)");
        db.execSQL("create table suppliers (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, phone TEXT, email TEXT UNIQUE)");

        Supplier supplier = new Supplier("Wędliny ABC", "650320444", "wedliny@gmail.com");
        Supplier supplier2 = new Supplier("Kasia Mleczna", "700343222", "mlekowita@gmail.com");
        Supplier supplier3 = new Supplier("Warzywniak Świeży", "650320444", "warzywa@gmail.com");

        Product product = new Product("Salami", 20, supplier.objectId);
        Product product2 = new Product("Kiełbasa", 8, supplier.objectId);
        Product product3 = new Product("Szynka", 10, supplier.objectId);
        Product product4 = new Product("Boczek 2kg", 80, supplier.objectId);
        Product product5 = new Product("Pierś z kurczaka", 30, supplier.objectId);

        Product product6 = new Product("Ser", 4, supplier2.objectId);
        Product product7 = new Product("Mleko 2l", 15, supplier2.objectId);
        Product product8 = new Product("Twaróg", 2, supplier2.objectId);
        Product product9 = new Product("Jogurt", 10, supplier2.objectId);

        Product product10 = new Product("Pomidory 3kg", 20, supplier3.objectId);
        Product product11 = new Product("Szpinak", 20, supplier3.objectId);
        Product product12 = new Product("Marchew", 10, supplier3.objectId);
        Product product13 = new Product("Kapusta", 6, supplier3.objectId);

        ContentValues cv = new ContentValues();
        cv.put("_id", product.objectId);
        cv.put("name", product.getPostTitle());
        cv.put("price", product.getPrice());
        cv.put("supplier_id", product.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product2.objectId);
        cv.put("name", product2.getPostTitle());
        cv.put("price", product2.getPrice());
        cv.put("supplier_id", product2.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product3.objectId);
        cv.put("name", product3.getPostTitle());
        cv.put("price", product3.getPrice());
        cv.put("supplier_id", product3.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product4.objectId);
        cv.put("name", product4.getPostTitle());
        cv.put("price", product4.getPrice());
        cv.put("supplier_id", product4.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product5.objectId);
        cv.put("name", product5.getPostTitle());
        cv.put("price", product5.getPrice());
        cv.put("supplier_id", product5.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product6.objectId);
        cv.put("name", product6.getPostTitle());
        cv.put("price", product6.getPrice());
        cv.put("supplier_id", product6.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product7.objectId);
        cv.put("name", product7.getPostTitle());
        cv.put("price", product7.getPrice());
        cv.put("supplier_id", product7.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product8.objectId);
        cv.put("name", product8.getPostTitle());
        cv.put("price", product8.getPrice());
        cv.put("supplier_id", product8.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product9.objectId);
        cv.put("name", product9.getPostTitle());
        cv.put("price", product9.getPrice());
        cv.put("supplier_id", product9.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product10.objectId);
        cv.put("name", product10.getPostTitle());
        cv.put("price", product10.getPrice());
        cv.put("supplier_id", product10.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product11.objectId);
        cv.put("name", product11.getPostTitle());
        cv.put("price", product11.getPrice());
        cv.put("supplier_id", product11.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product12.objectId);
        cv.put("name", product12.getPostTitle());
        cv.put("price", product12.getPrice());
        cv.put("supplier_id", product12.getSupplierId());
        db.insert("products", null, cv);

        cv.put("_id", product13.objectId);
        cv.put("name", product13.getPostTitle());
        cv.put("price", product13.getPrice());
        cv.put("supplier_id", product13.getSupplierId());
        db.insert("products", null, cv);




    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // implement schema changes and data massage here when upgrading
    }

}