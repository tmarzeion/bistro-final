package com.biokami.wojtek.bistro;

/**
 * Created by nirav on 21/02/16.
 */
public class Product {

    private static int counter = 0;

    public final int objectId;

    public int getPrice() {
        return price;
    }

    public int getSupplierId() {
        return supplierId;
    }

    private int price;

    private String postTitle;

    private int supplierId;

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

//    public int supplierId;

    public Product(String postTitle, int price, int supplierId) {
        this.objectId = counter++;
        this.postTitle = postTitle;
        this.price = price;
        this.supplierId = supplierId;
//        this.supplierId = supplier.objectId;
    }
}
