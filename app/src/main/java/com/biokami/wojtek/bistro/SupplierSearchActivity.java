package com.biokami.wojtek.bistro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SupplierSearchActivity extends AppCompatActivity {

    private ListView listView;
    private MyAppAdapter myAppAdapter;
    public static ArrayList<Supplier> supplierArrayList;

//    public static void addSupplier(String name){
//        supplierArrayList.add(new Supplier(name));
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.supplier_activity_search);
        listView= (ListView) findViewById(R.id.listView);
        supplierArrayList =new ArrayList<>();
//        supplierArrayList.add(new Supplier("Bożena"));
//        supplierArrayList.add(new Supplier("Warzywa ABC"));
//        supplierArrayList.add(new Supplier("Wędliny Karola"));
//        supplierArrayList.add(new Supplier("Wiejski nabial"));


        // TodoDatabaseHandler is a SQLiteOpenHelper class connecting to SQLite
        SQLiteOpenHelper handler = new SQLHelper(this);
// Get access to the underlying writeable database
        SQLiteDatabase db = handler.getWritableDatabase();
// Query for items from the database and get a cursor back
        Cursor todoCursor = db.rawQuery("SELECT  * FROM suppliers", null);

        // Find ListView to populate
        ListView lvItems = (ListView) findViewById(R.id.listView);
// Setup cursor adapter using cursor from last step
        SupplierCursorAdapter todoAdapter = new SupplierCursorAdapter(this, todoCursor);
// Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);

//        myAppAdapter=new MyAppAdapter(supplierArrayList,SupplierSearchActivity.this);
//        listView.setAdapter(myAppAdapter);
    }

    public class MyAppAdapter extends BaseAdapter {

        public class ViewHolder {
            TextView txtTitle,txtSubTitle;
            ImageButton imgEdit;


        }

        public List<Supplier> parkingList;

        public Context context;
        ArrayList<Supplier> arraylist;

        private MyAppAdapter(List<Supplier> apps, Context context) {
            this.parkingList = apps;
            this.context = context;
            arraylist = new ArrayList<Supplier>();
            arraylist.addAll(parkingList);

        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            ViewHolder viewHolder;

            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.supplier_item, null);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.txtTitle = (TextView) rowView.findViewById(R.id.title);
//                viewHolder.txtSubTitle = (TextView) rowView.findViewById(R.id.subtitle);
//                viewHolder.imgEdit = (ImageButton) rowView.findViewById(R.id.edit_supplier);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.txtTitle.setText(parkingList.get(position).getName() + "");
//            viewHolder.txtSubTitle.setText(parkingList.get(position).getPostSubTitle() + "");
            return rowView;


        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            parkingList.clear();
            if (charText.length() == 0) {
                parkingList.addAll(arraylist);

            } else {
                for (Supplier supplierDetail : arraylist) {
                    if (charText.length() != 0 && supplierDetail.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        parkingList.add(supplierDetail);
                    }
//
//                    else if (charText.length() != 0 && supplierDetail.getPostSubTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
//                        parkingList.add(supplierDetail);
//                    }
                }
            }
            notifyDataSetChanged();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.supplier_menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                myAppAdapter.filter(searchQuery.toString().trim());
                listView.invalidate();
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }
        if (id == R.id.action_switch_to_product) {
            Intent intent = new Intent(this, ProductSearchActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.add_new_supplier) {
            Intent intent = new Intent(this, AddNewSupplier.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void test(View view){
        Context context = getApplicationContext();
        CharSequence text = "Settings";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
    public void test2(View view){
        Context context = getApplicationContext();
        CharSequence text = "Trash!";
        int duration = Toast.LENGTH_SHORT;

//        Toast toast = Toast.makeText(context, text, duration);
//        toast.show();

        Intent intent = new Intent(this, ProductSearchActivity.class);
//        EditText editText = (EditText) findViewById(R.id.editText);
//        String message = editText.getText().toString();
//        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }


}
