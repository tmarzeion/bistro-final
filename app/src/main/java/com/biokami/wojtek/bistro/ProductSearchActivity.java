package com.biokami.wojtek.bistro;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProductSearchActivity extends AppCompatActivity {

    private ListView listView;
    private MyAppAdapter myAppAdapter;
    public ArrayList<Product> productArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_activity_search);
        listView= (ListView) findViewById(R.id.listView);
        productArrayList =new ArrayList<>();


        // TodoDatabaseHandler is a SQLiteOpenHelper class connecting to SQLite
        SQLiteOpenHelper handler = new SQLHelper(this);
// Get access to the underlying writeable database
        SQLiteDatabase db = handler.getWritableDatabase();
// Query for items from the database and get a cursor back
        Cursor todoCursor = db.rawQuery("SELECT  * FROM products", null);

        // Find ListView to populate
        ListView lvItems = (ListView) findViewById(R.id.listView);
// Setup cursor adapter using cursor from last step
        ProductCursorAdapter todoAdapter = new ProductCursorAdapter(this, todoCursor);
// Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);


//        db.execSQL("create table products (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, price INTEGER, supplier_id INTEGER)");
//        db.execSQL("create table suppliers (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, phone TEXT, email TEXT UNIQUE)");

//        try {
//            SQLiteOpenHelper dbHelper = new SQLHelper(this);
//            SQLiteDatabase db = dbHelper.getReadableDatabase();
//            Cursor  cursor = db.query(("PRODUCTS",
//                    new String[]{"_id", "NAME"},
//                    null, null, null, null);

//            if(cursor.moveToFirst()){
//                int _id = cursor.getInt(0);
//                String name = cursor.getString(1);
//                int price = cursor.getInt(2);
//                int supplier_id = cursor.getInt(3);
//            }
//            CursorAdapter listAdapter = new SimpleCursorAdapter(this, R.layout.product_item, cursor, new String[]{"name"},
//                    new int[]{R.id.nameSubtitle});
//            listView.setAdapter(listAdapter);
//            cursor.close();
//            db.close();
//        } catch(SQLiteException e){
//            Toast toast = Toast.makeText(this, "Baza danych jest niedostępna", Toast.LENGTH_SHORT);
//            toast.show();
//        }


//        myAppAdapter=new MyAppAdapter(productArrayList,ProductSearchActivity.this);
//        listView.setAdapter(myAppAdapter);
    }



    public class MyAppAdapter extends BaseAdapter {

        public class ViewHolder {
            TextView txtTitle,txtSubTitle;


        }

        public List<Product> parkingList;

        public Context context;
        ArrayList<Product> arraylist;

        private MyAppAdapter(List<Product> apps, Context context) {
            this.parkingList = apps;
            this.context = context;
            arraylist = new ArrayList<Product>();
            arraylist.addAll(parkingList);

        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            ViewHolder viewHolder;

            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.product_item, null);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.txtTitle = (TextView) rowView.findViewById(R.id.title);
//                viewHolder.txtSubTitle = (TextView) rowView.findViewById(R.id.subtitle);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.txtTitle.setText(parkingList.get(position).getPostTitle() + "");
//            viewHolder.txtSubTitle.setText(parkingList.get(position).getPostSubTitle() + "");
            return rowView;


        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            parkingList.clear();
            if (charText.length() == 0) {
                parkingList.addAll(arraylist);

            } else {
                for (Product productDetail : arraylist) {
                    if (charText.length() != 0 && productDetail.getPostTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                        parkingList.add(productDetail);
                    }

                }
            }
            notifyDataSetChanged();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                myAppAdapter.filter(searchQuery.toString().trim());
                listView.invalidate();
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }

        if (id == R.id.action_switch_to_supplier) {
            Intent intent = new Intent(this, SupplierSearchActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.add_new_product) {
            Intent intent = new Intent(this, AddNewProduct.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void test(View view){
        Context context = getApplicationContext();
        CharSequence text = "test1";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
    public void test2(View view){
        Intent intent = new Intent(this, SupplierSearchActivity.class);
        startActivity(intent);
    }

//  db.execSQL("create table products (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, price INTEGER, supplier_id INTEGER)");
//  db.execSQL("create table suppliers (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, phone TEXT, email TEXT UNIQUE)");




}
