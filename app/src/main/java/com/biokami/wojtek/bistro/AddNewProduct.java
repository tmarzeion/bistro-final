package com.biokami.wojtek.bistro;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddNewProduct extends AppCompatActivity {

    private EditText productName;
    private EditText productPrice;
    private EditText supplierId;

    private Boolean ispricevalid;
    private ListView productList;

    private ArrayList<String> spinnerArray = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_product);

        productList = (ListView) findViewById(R.id.productsList);
//        ImageButton btn = new ImageButton(this);
//        btn.setImageResource(R.drawable.add_plus);
//        btn.setBackgroundColor(Color.parseColor("#50BE86"));
//        productList.addHeaderView(btn);

        Spinner spinner = (Spinner) findViewById(R.id.spinnerOfSuppliers);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

    }


    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    public void addSupplier(View view){
        productName = (EditText) findViewById(R.id.supplierTextName);
        if(ispricevalid == true && !isEmpty(productName)){
            Context context = getApplicationContext();
            CharSequence text = "Jest oki.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }else {
            Context context = getApplicationContext();
            CharSequence text = "Niepoprawna cena lub nazwa.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }


}
