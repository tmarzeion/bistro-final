package com.biokami.wojtek.bistro;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddNewSupplier extends AppCompatActivity {

    private EditText supplierPhone;
    private EditText supplierEmail;
    private EditText supplierName;
    private Boolean isnumbervalid;
    private Boolean isemailvalid;
    private ListView productList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_supplier);

        productList = (ListView) findViewById(R.id.productsList);
//        ImageButton btn = new ImageButton(this);
//        btn.setImageResource(R.drawable.add_plus);
//        btn.setBackgroundColor(Color.parseColor("#50BE86"));
//        productList.addHeaderView(btn);

        supplierEmail = (EditText) findViewById(R.id.supplierEmailText);
        supplierEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!isEmailValid(s.toString())){
                    isemailvalid=false;
                    supplierEmail.setHighlightColor(Color.parseColor("#F44336"));
                    supplierEmail.setTextColor(Color.parseColor("#F44336"));
                }else {
                    isemailvalid=true;
                    supplierEmail.setTextColor(Color.parseColor("#50BE86"));
                }
            }



            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        supplierPhone = (EditText) findViewById(R.id.supplierPhoneText);
        supplierPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!isValidNumber(s.toString())){
                    isnumbervalid=false;
                    supplierPhone.setHighlightColor(Color.parseColor("#F44336"));
                    supplierPhone.setTextColor(Color.parseColor("#F44336"));
                }else {
                    isnumbervalid=true;
                    supplierPhone.setTextColor(Color.parseColor("#50BE86"));
                }
            }



            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


//    Validetors

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches();
    }
    //
//    boolean validateNewSupplier(View view){
//        Context context = getApplicationContext();
//
//    }
    private boolean isValidNumber(String phonenumber) {
        String PHONE_PATTERN = "^[0-9]{9}$";

        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(phonenumber);
        return matcher.matches();
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    public void editSupplier(View view){



//        Context context = getApplicationContext();
//        CharSequence text = "Trash!";
//        int duration = Toast.LENGTH_SHORT;
//        Toast toast = Toast.makeText(context, text, duration);
//        toast.show();
//        TextView title = (TextView) view.findViewById(R.id.title);

    }

    public void addSupplier(View view){
        supplierName = (EditText) findViewById(R.id.supplierTextName);
        if(isemailvalid == true && isnumbervalid == true && !isEmpty(supplierName)){
            Context context = getApplicationContext();
            CharSequence text = "Jest oki.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }else {
            Context context = getApplicationContext();
            CharSequence text = "Niepoprawny email lub telefon.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }


}
